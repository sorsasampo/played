#!/usr/bin/env python3
# Copyright (c) 2017-2018 Sampo Sorsa <sorsasampo@protonmail.com>

import contextlib
from datetime import datetime, timedelta
import io
import unittest
from unittest.mock import ANY, MagicMock, patch

import played


class CommonFixture(unittest.TestCase):
    def setUp(self):
        self.db = played.Database(':memory:')
        self.db.migrate()

    def insertSession(self, game_name, ts_start, seconds=1):
        game_id = self.db.get_game_id(game_name)
        exe_id = self.db.get_or_create_exe(game_id, 'unittest', '')

        ts_end = ts_start + timedelta(seconds=seconds)
        self.db.add_entry(exe_id, ts_start, ts_end, 0, 0)


class TestDatabase(unittest.TestCase):
    def test_init_db(self):
        db = played.Database(':memory:')
        db.migrate()


class TestIdleTime(unittest.TestCase):
    def setUp(self):
        self.idle = played.IdleTime()
        self.idle.get_current_idle_time = MagicMock()

    def test_idle_time(self):
        for i in range(76):
            self.idle.get_current_idle_time.return_value = i * 1000
            self.idle.tick()
        for i in range(11):
            self.idle.get_current_idle_time.return_value = i * 1000
            self.idle.tick()
        self.assertEqual(int(self.idle.idle_total), 75)

    def test_idle_time_at_end(self):
        self.idle.get_current_idle_time.return_value = 75000
        self.idle.start()
        self.idle.stop()
        self.idle.join()  # wait for thread to handle stop and exit
        self.assertEqual(int(self.idle.idle_total), 75)


class TestLast(CommonFixture):
    def test_last(self):
        self.insertSession('hidden', datetime(1999, 1, 1))
        self.insertSession('A', datetime(2000, 1, 1))
        self.insertSession('B', datetime(2000, 1, 2, microsecond=5))
        self.insertSession('A', datetime(2000, 1, 3))

        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            played.last(self.db, 2)
        self.assertEqual(
            f.getvalue(),
            '2000-01-02 00:00 B\n'
            '2000-01-03 00:00 A\n'
        )


class TestMisc(unittest.TestCase):
    def test_secs2time(self):
        self.assertEqual('  1d  2h  3m  4s', played.secs2time(1 * 86400 + 2 * 3600 + 3 * 60 + 4))


class TestPlay(CommonFixture):
    def test_known_exe(self):
        """
        Test that known executable does not create new exe, but creates new entry.
        """
        played.play(self.db, 'true', '/bin/true', ['arg'])
        played.play(self.db, 'true', '/bin/true', ['arg'])

        self.db.c.execute('SELECT * FROM exe')
        exe_rows = self.db.c.fetchall()
        self.assertEqual(len(exe_rows), 1)
        self.assertEqual(tuple(exe_rows[0]), (1, 1, '/bin/true', 'arg'))

        self.db.c.execute('SELECT id, exe_id, idle, exitcode FROM entry')
        entry_rows = self.db.c.fetchall()
        self.assertEqual(len(entry_rows), 2)
        self.assertEqual(tuple(entry_rows[0]), (1, 1, 0, 0))
        self.assertEqual(tuple(entry_rows[1]), (2, 1, 0, 0))

    def test_new_game(self):
        played.play(self.db, 'true', '/bin/true', ['dummy'])

        self.db.c.execute('SELECT * FROM exe')
        exe_rows = self.db.c.fetchall()
        self.assertEqual(len(exe_rows), 1)
        self.assertEqual(tuple(exe_rows[0]), (1, 1, '/bin/true', 'dummy'))

        self.db.c.execute('SELECT id, exe_id, idle, exitcode FROM entry')
        entry_rows = self.db.c.fetchall()
        self.assertEqual(len(entry_rows), 1)
        self.assertEqual(tuple(entry_rows[0]), (1, 1, 0, 0))

    def test_path_resolving(self):
        played.play(self.db, 'true', '/bin/true', [])
        played.play(self.db, 'true', 'true', [])

        self.db.c.execute('SELECT COUNT(*) FROM exe')
        exe_rows = self.db.c.fetchone()[0]
        self.assertEqual(1, exe_rows)

    def test_play(self):
        played.play(self.db, 'true', '/bin/true', ['first'])
        played.play(self.db, 'true', '/bin/true', ['second', 'call'])
        with patch('played.track') as mock_track:
            played.play(self.db, 'true', None, None)
            mock_track.assert_called_once_with(self.db, ANY, '/bin/true', ['second', 'call'])

    def test_play_failed(self):
        with patch('played.track') as mock_track:
            played.play(self.db, 'invalid', None, None)
            mock_track.assert_not_called()

    def test_play_with_exe(self):
        played.play(self.db, 'true', 'true', ['arg'])
        with patch('played.track') as mock_track:
            played.play(self.db, 'true', None, None)
            mock_track.assert_called_once_with(self.db, ANY, '/bin/true', ['arg'])


class TestSessions(CommonFixture):
    def test_sessions(self):
        ts_start = datetime(2000, 1, 1)
        ts_end = datetime(2000, 1, 1, 1, 2, 3)
        self.db.c.execute("INSERT INTO game(id, name) VALUES(1, 'test')")
        self.db.c.execute("INSERT INTO exe(id, game_id, path, args) VALUES(1, 1, '/unit/test', '')")
        self.db.c.execute("INSERT INTO entry(exe_id, ts_start, ts_end, exitcode) VALUES(1, ?, ?, 0)", (ts_start, ts_end))
        entries = self.db.get_entries(None)
        self.assertEqual(len(entries), 1)
        self.assertEqual(entries[0]['secs'], 3600 + 2 * 60 + 3)


class TestStats(CommonFixture):
    def test_stats(self):
        played.stats(self.db, None)

    def test_stats_limit(self):
        self.insertSession('A', datetime(2000, 1, 1))
        self.insertSession('B', datetime(2000, 1, 2), seconds=10)

        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            played.stats(self.db, limit=1)

        self.assertRegex(
            f.getvalue(),
            '^B *1 * 0d  0h  0m 10s$',
            'Only most played game is shown'
        )


if __name__ == '__main__':
    unittest.main(buffer=True)
