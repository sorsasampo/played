# played

`played` tracks time games have been played for.

# Examples

`stats` command shows number of sessions and total played for all games or a
single one:

    $ played stats Hearthstone
    Hearthstone                                27   0d 15h 26m 18s

`play <game> [executable [args]]` starts a game using the last executable and
arguments provided, and outputs some statistics after it exits:

    $ played play Hearthstone
    ...
    Played within:
    Today:     0d  1h 30m  9s, 4 sessions
     7 days:   0d  5h 39m 11s, 10 sessions
    14 days:   0d 10h 16m 40s, 19 sessions

# Dependencies

* python3-xdg

# Installation

    git clone https://gitlab.com/sorsasampo/played.git
    ln -rs played/played.py ~/bin/played

Assuming `~/bin/` exists and is already in your `$PATH`.

To install the dependencies on Debian:

    sudo apt install python3-xdg

# FAQ

## Can I track playtime of Lutris games?

Yes! Find out the game id with `lutris -lo`, and run the game with:

    lutris lutris:rungame/gameid

Refer to Lutris' [command line options][lutris-cli] for details.

NOTE: Until [lutris-578][] is fixed Lutris main UI will show up and you will
have to close it manually after exiting the game.

## Can I track playtime of Steam games?

`steam -applaunch <steamgameid>` can be used to launch Steam games.
Unfortunately if Steam is already running, this command will exit immediately.
And if Steam is not running, you would have to quit it before the playtime is
tracked by played.

### Option 1: leverage Lutris

Better way is to leverage Lutris' game tracking, and launch Steam games using
Lutris. See above.

### Option 2: set launch options in Steam

Open Properties of a game and choose `Set launch options...`:

    played play SomeGameName %command%

Downside: you don't see played output, because there is no console.

See also: [ArchWiki: Steam launch options][archwiki-steam-launch]

[archwiki-steam-launch]: https://wiki.archlinux.org/index.php/steam#Launch_options
[lutris-578]: https://github.com/lutris/lutris/issues/578
[lutris-cli]: https://github.com/lutris/lutris#command-line-options
